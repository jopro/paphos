#provides a very simple framework for performing genetic generation of algorithms
from random import choice, randrange

class Genome:
    def __init__(self, options, length, mutation, parents=[]):
        self.dna = range(length)
        for i in self.dna:
            if randrange(0, 1) > mutation or len(parents) == 0:
                o = choice(options)
            else:
                o = choice(parents)[i]#take this dna string from the parents
            self.dna[i] = o

class Evolution():
    def __init__(self, options, mutationP = 0.3, populationSize = 1000, genomeLength = 20):
        self.options = options
        self.mutationP = mutationP
        self.populationSize = populationSize
        self.genomeLength = genomeLength
        self.generation = 0
        self.currentGen = []
        for i in range(populationSize):
            genome = Genome(options, genomeLength, mutationP)
            self.currentGen.append(genome)
        self.getBest() 

    def fitness(self, dna):#returns 0..1
        return 1#evaluate the fitness of the  genome

    def getBest(self):
        self.currentGen = sorted(self.currentGen, key=lambda genome: self.fitness(genome.dna))
        return self.currentGen[0]

    def solve(self):
        solved = 1
        while solved > 0:
            self.nextGeneration()
            first = self.currentGen[0].dna
            solved = self.fitness(first)
            print str(self.generation)+":"+str(1-solved)
            print first
        return first
        
    def nextGeneration(self):
        self.generation+=1
        best = self.currentGen[0:len(self.currentGen)/4]
        #make new genomes
        newGenomes = range(self.populationSize-len(best))
        for gen in newGenomes:
            parents = [choice(best).dna, choice(best).dna]
            if randrange(0, 100) == 0 or parents[0] == parents[1]:
                parents = []
            genome = Genome(self.options, self.genomeLength, self.mutationP, parents)
            newGenomes[gen] = genome
        newGenomes.extend(best)
        self.currentGen = newGenomes #update the generation
        self.getBest()#sort by fitness
