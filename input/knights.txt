# RULES #
((?P says ?X is_a ?Y) (?P is_a knight) implies (?X is_a ?Y))
((?P says ?X is_a ?Y) (?P is_a knave) implies (?X is_not_a ?Y))

((?X is_a knight) implies (?X is_not_a knave))
((?X is_a knave) implies (?X is_not_a knight))
((?X is_not_a knight) implies (?X is_a knave))
((?X is_not_a knave) implies (?X is_a knight))

# PROBLEM #
(B says B is_a knight)
(A says B is_a knave)
