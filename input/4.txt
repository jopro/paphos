((?R is_a transitive_relation) (?X ?R ?Y) (?Y ?R ?Z) implies (?X ?R ?Z))
((?R is_a symmetric_relation) (?X ?R ?Y) implies (?Y ?R ?X))
((?R is_a antisymmetric_relation) (?X ?R ?Y) (?Y ?R ?X) implies (?X is ?Y))

((?R is_a equivalence_relation) implies (?R is_a reflexive_relation) (?R is_a symmetric_relation) (?R is_a transitive_relation))
((?R is_a reflexive_relation) (?R is_a symmetric_relation) (?R is_a transitive_relation) implies (?R is_a equivalence_relation))

((?R1 is_inverse_relation_of ?R2) implies (?R2 is_inverse_relation_of ?R1))
((?R1 is_inverse_relation_of ?R2) (?X ?R1 ?Y) implies (?Y ?R2 ?X))

(is_a is_a transitive_relation)

(is is_a equivalence_relation)

(is_in is_a transitive_relation)


#testing

(is_parent_of is_inverse_relation_of is_child_of)

(pete is_parent_of clara)
