class Neuron:
    def __init__(self, inputs=None):
        self.inputs = inputs
        if self.inputs == None:
            self.inputs = {}
        self.override = None
        self.label = ""

    def setInput(self, inputN, weight):
        if weight != 0:
            self.inputs[inputN] = weight
        else:
            del self.inputs[inputN]

    def setValue(self, value):
        self.override = value

    def getValue(self):
        if self.override != None:
            return self.override
        
        value = 0
        for node, weight in self.inputs.items():
            value += node.getValue()*weight
        
        if value > 0:
            return 1
        return 0
        
    def __str__(self):
        o = ""
        if self.override == None:
            o = [str(node)+"*"+str(weight) for node, weight in self.inputs.items()]
            o = "("+"+".join(o)+") = "
        o += str(self.getValue())
        return self.label+o
    
class NeuralNet:
    def __init__(self, numNodes):
        self.nodes = [Neuron() for i in range(numNodes+1)]
        self.nodes[-1].setValue(1)

    def getV(self, nodeID):
        return self.nodes[nodeID].getValue()

    def setV(self, nodeID, value):
        self.nodes[nodeID].setValue(value)

    def setEdge(self, nodeID, inNodeID, weight):
        inNode = self.nodes[inNodeID]
        self.nodes[nodeID].setInput(inNode, weight)

    def getINodes(self):
        return [node for node in self.nodes[:-1] if node.inputs == {}]
    
    def getONodes(self):
        outputs = [node for node in self.nodes[:-1] if node.override == None]
        for node in self.nodes[:-1]:
            for inN in node.inputs.keys():
                if inN in outputs:
                    outputs.remove(inN)
        return outputs

    def test(self, inputNodes=None, depth=0):
        if inputNodes == None:
            inputNodes = self.getINodes()
        
        if inputNodes == []:
            oNodes = self.getONodes()
            for n in oNodes:
                print("\t"*depth+str(n))
            return
        else:
            ins = [0, 1]
            for value in ins:
                inputNodes[0].setValue(value)
                print("\t"*depth+"I "+str(inputNodes[0]))
                self.test(inputNodes[1:], depth+1)

def main():
    net = NeuralNet(4)
    net.setEdge(2, 0, 1)
    net.setEdge(2, 1, 1)
    net.setEdge(2, -1, -0.5)
    net.nodes[2].label = "OR "
    
    net.setEdge(3, 0, 1)
    net.setEdge(3, 1, 1)
    net.setEdge(3, -1, -1.5)
    net.nodes[3].label = "AND "
    net.test()

if __name__ == "__main__":
    main()
