#TO DO#
##DOC AND INTERFACE##
- Update the site

##NEW FEATURES##
- Probability (not, or, and, nand, xor)
- Question asking?
- Variables/State in logical statements
- Look at using Decision Trees, Q-Learning and Neural Networks for finding patterns
- Compression and Reduction of knowledge that is not going to be used (in a way that it can still be used...)

##IMPROVEMENTS##
- Activation of functions
    - Clean up the functions interface...
    - Holding data (escaping and other problems) 
    - Built in functions for updating and stuff?
        - Write files
        - Read Files
    - Calling arbitrary python (Should this be done by writing functions that are called by the function interface?)
        - Hardware interfaces?
        - Live importing (__import__)
- Work on importing .json-ld files, rdf, owl etc. to interface with the semantic web

##TESTING AND VERIFICATION##
- Unit Tests
- Test goal making and solving
- Pass pylint tests with no errors or warnings
