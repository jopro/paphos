"""A class for representing propositions"""

SEPERATOR = " "

class Prop(object):
    """A proposition is a set of nodes"""
    def __init__(self, nodes):
        self.nodes = nodes
        self.abstract = False
        for node in self.nodes:
            if "?" in str(node):
                self.abstract = True
                break

    def assign(self, condition, match):
        """replaces abstract nodes with nodes from the match:
        where self.nodes[x] == condition.nodes[i]
            self.nodes[x] = match.nodes[i]"""
        if not self.abstract:
            return self#cannot overwrite concrete nodes
        #replace abstracts with concrete nodes from the match
        new_nodes = list(self.nodes)
        for index in range(len(new_nodes)):
            node = new_nodes[index]
            if not isinstance(node, str) and not isinstance(node, unicode):
                node = node.assign(condition, match)
                if node == None:
                    return
            elif node in condition.nodes:
                match_index = condition.nodes.index(node)
                if match_index >= len(match.nodes):
                    return
                match_node = match.nodes[match_index]
                if node[0] == "?":
                    node = match_node
                elif node != match_node:
                    return
            new_nodes[index] = node
        return Prop(new_nodes)

    def is_rule(self):
        """returns a boolean: true if the proposition is an abstract rule,
        false if the proposition is a statement, condition or outcome"""
        return "implies" in self.nodes

    def __eq__(self, other):
        return str(self) == str(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "("+(SEPERATOR).join([str(node) for node in self.nodes])+")"

def tests():
    """Some unit tests for proposition functions"""
    prop1 = Prop(["?X", "?R", "?Y"])
    prop2 = Prop(["?Y", "?R", "?X"])
    prop = Prop([prop1, "implies", prop2])
    rep = Prop(["a", "?R", "c"])

    new_prop = prop.assign(prop1, rep)
    print prop
    print rep
    print new_prop

if __name__ == "__main__":
    tests()
