"""Extra functions to be called by the engine to allow for
callable code in the logic"""

try:
    from termcolor import cprint
except ImportError:
    def cprint(text, color='white'):
        print text

def out(*args):
    outText = " ".join([str(arg) for arg in args])
    cprint(outText, 'green')
    return outText

def outv(*args):
    outText = " ".join([str(arg.value) for arg in args])
    cprint(outText, 'green')
    return outText

def add(numA, numB):
    return float(numA.value)+float(numB.value)

def subtract(numA, numB):
    return float(numA.value)-float(numB.value)

def multiply(numA, numB):
    return float(numA.value)*float(numB.value)

def divide(numA, numB):
    return float(numA.value)/float(numB.value)

def power(numA, numB):
    return float(numA.value)**float(numB.value)

def root(numA, numB):
    return float(numA.value)**(1.0/float(numB.value))

def getinput(prompt):
    return raw_input(str(prompt.value))

def modulo(numA, numB):
    return float(numA.value)%float(numB.value)

def setNodeValue(value):
    return value

def importLib(lib):
    return __import__(lib)

functions = {
    'out':out,
    'outv':outv,
    'add':add,
    'subtract':subtract,
    'multiply':multiply,
    'divide':divide,
    'power':power,
    'root':root,
    'getinput':getinput,
    'modulo':modulo,
    'set':setNodeValue,
    'importLib':importLib
    }
