from paphos_engine.prop import Prop
from pymongo import MongoClient

class Transform(object):
    def encode(self, data, removeAbstracts=False):
        if isinstance(data, Prop):
            data = self.encode_root(data, removeAbstracts)
        elif isinstance(data, list) or isinstance(data, tuple):
            data = [self.encode(item, removeAbstracts) for item in data]
        elif isinstance(data, dict):
            for (key, value) in data.items():
                data[key] = self.encode(value, removeAbstracts)
        return data

    def decode(self, son):
        if isinstance(son, dict):
            for (key, value) in son.items():
                son[key] = self.decode(value)
            if son.get("_type") == "Prop":
                son = self.decode_root(son)
        if isinstance(son, list) or isinstance(son, tuple):
            son = [self.decode(son_item) for son_item in son]
        return son

    def encode_root(self, prop, removeAbstracts=False):
        d = {
                "_type":"Prop",
                "_id":str(prop),
                "len":len(prop.nodes),
                "abstract":prop.abstract
            }

        nodes = []
        for nodeIndex in range(len(prop.nodes)):
            node = prop.nodes[nodeIndex]
            if isinstance(node, Prop):
                node = self.encode_root(node)
            if not(removeAbstracts and node[0] == "?"):
                d["node_"+str(nodeIndex)] = node
                nodes.append(node)
        d["nodes"] = nodes
        return d

    def decode_root(self, son):
        nodes = []
        length = son["len"]
        for index in range(length):
            node = son["node_"+str(index)]
            if isinstance(node, dict):
                node = self.decode_root(node)
            nodes.append(node)
        return Prop(nodes)

    def getIDs(self, nodes):
        ids = [node.ID for node in nodes]
        return ids

class KB(object):
    def __init__(self):
        self.transform = Transform()
        self.client = MongoClient()#("mimir.com")
        self.db = self.client.local
        
        #self.db.authenticate('user', 'password', mechanism='SCRAM-SHA-1')

        self.props = self.db.props

    def add(self, prop):#Adds a prop to the kb
        propEncode = self.transform.encode(prop)
        if self.getProp(prop) == None:
            propEncode["unprocessed"] = True
            self.props.insert_one(propEncode)

    def get(self, toFind=None):#queries the kb
        if toFind == None:
            toFind = {}
        cursor = self.props.find(toFind)
        cursor = list(cursor)
        return self.transform.decode(cursor)

    def findMatches(self, abstractProp):#finds props that match prop with abstracts
        query = self.transform.encode(abstractProp)
        nodeMatch = "node_"
        del query["_id"]
        del query["nodes"]
        query["abstract"] = False
        for key, value in query.items():
            #if the key is a node and the value is abstract
            if key[0:len(nodeMatch)] == nodeMatch and value[0] == "?":
                del query[key]
        return self.get(query)

    def findConditions(self, prop):#finds abstract props that match the prop
        query = {"abstract":True, "len":len(prop.nodes)}#this needs performance improvements!!! (by restricting the query)
        return self.get(query)

    def getProp(self, prop):#gets one prop from the kb
        #gets a matching proposition from the kb
        match = self.props.find_one({"_id":str(prop)})
        return self.transform.decode(match)

    def getRules(self, cond):#gets the rules that contains the condition
        rules = list(self.props.find({"nodes":{"$elemMatch":{"_id":str(cond)}}}))
        return self.transform.decode(rules)

    def getPropToProcess(self, prop=None):#gets a prop from the to process queue
        query = {"unprocessed":True}
        if prop != None: query["_id"] = str(prop)
        prop = self.props.find_one(query)
        return self.transform.decode(prop)

    def finishedProcess(self, prop):#updates a prop makring it processed
        query = {"_id":str(prop)}
        self.props.update_one(query, {"$unset":{"unprocessed":True}})
