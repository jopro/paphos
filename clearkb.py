"""Clears the mongo KB"""
from pymongo import MongoClient

def main():
    "Connects to the KB and removes all documents"
    database = MongoClient().local
    props = database.props
    props.delete_many({})

if __name__ == "__main__":
    main()
