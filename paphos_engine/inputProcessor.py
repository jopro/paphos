from paphos_engine.prop import SEPERATOR, Prop
from paphos_engine.extra_functions import cprint
import signal
import readline
import sys

class inputProcessor:
    def __init__(self, bot):
        self.bot = bot
        readline.set_completer(self.complete)
        readline.parse_and_bind('tab: complete')
        self.options = []

    def filterInput(self, inT):
        allLines = inT.replace("\t", "").split("\n")
        lines = [line for line in allLines if line.strip() != ""]
        lines = [line.split("#")[0] for line in lines]
        return lines

    def processFile(self, filename):
        infile = self.filterInput(open(filename).read())
        for line in infile:
            self.process(line)

    def processInput(self):
        # Make compatible with Python 2.x. and 3.x.
        try: readIn = raw_input
        except NameError: readIn = input
        signal.signal(signal.SIGINT, signal_handler)
        
        inputLine = None
        while True:
            inputLine = self.filterInput(readIn(""))
            if inputLine == "(done)":
                return
            
            for line in inputLine:
                if len(line) > 0 and (line[0] == "/" or line[0] == "?"):
                    results = self.bot.findChains(line[1:], line[0] == "?")
                    results = map(str, results)
                    cprint(">"+"\n>".join(results), "green")
                else:
                    self.process(line)
    
    def complete(self, text, state):
        if state == 0:
            keys = sorted(self.bot.nodes.keys())
            for k in keys:
                if len(text) < len(k) and text == k[0:len(text)]:
                    self.options.append(k)
        if state < len(self.options):
            return self.options[state]
        return None

    def process(self, line):
        if SEPERATOR in line:
            nodes = self.processNodes(self.seperate(line))
            return self.bot.addProp(Prop(nodes))
        else:
            return line

    def processNodes(self, nodes):
        newNodes = []
        for node in nodes:
            if type(node) == str:
                if SEPERATOR in node:
                    node = self.process(node)
            newNodes.append(node)
        return newNodes

    def seperate(self, line):
        if line.count("(") != line.count(")"):
            print("MISSING PAREN IN "+line)
        out = []
        curr = ""
        nesting = 0
        for char in line:
            if nesting == 1:
                if (char == SEPERATOR or char == ")") and curr != "":
                    out.append(curr)
                    curr = ""
                elif char != SEPERATOR:
                    curr += char
            elif nesting > 1 or (char != "(" and char != ")"):
                if char != SEPERATOR or curr != "":
                    curr += char
            
            if char == "(": nesting += 1
            if char == ")": nesting -= 1
        return out

def signal_handler(signal, frame):
    print("")
    exit()

