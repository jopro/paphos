"""A worker for Paphos"""
from sys import argv
from paphos_engine import Bot

def main():
    """Boots up a Paphos worker and waits for input to process"""
    bot = Bot()
    flags = []
    for arg in argv[1:]:
        if arg[0] != "-":
            filename = arg
            bot.input.processFile(filename)
        else:
            flags.append(arg[1:])
    if 'c' not in flags:
        bot.input.processInput()
    else:
        bot.running = False
        bot.thought.join()
        exit()

if __name__ == "__main__":
    main()
