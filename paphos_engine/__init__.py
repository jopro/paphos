from paphos_engine.kb import KB
from paphos_engine.inputProcessor import inputProcessor
from paphos_engine.prop import Prop
from paphos_engine.extra_functions import cprint, functions
import threading

VERBOSE_DEBUG = False

class Bot:
    def __init__(self):
        self.kb = KB()
        self.input = inputProcessor(self)
        
        self.running = True
        self.thought = threading.Thread(target=self.process)
        self.thought.daemon = True
        self.thought.start()

    def process(self):
        while self.running:
            prop = self.kb.getPropToProcess()#get a prop
            if prop != None:
                print "PROCESSING "+str(prop)
                self.apply(prop)#apply it
                self.kb.finishedProcess(prop)

    def apply(self, prop):
        #adds the outcomes of the prop to the kb
        if prop.nodes[0] == "activate":
            print "F "+str(prop) #run the function
            
        if prop.is_rule():
            self.applyRule(prop)
        elif not prop.abstract:#find the rules that apply to this
            self.applyProp(prop)

    def addProp(self, prop):
        match = self.kb.getProp(prop)
        if self.kb.getProp(prop) == None:
            if not prop.abstract:
                print prop
            self.kb.add(prop)
            return prop
        #else: update probs
        return match

    def applyProp(self, prop):#find the rules
        conditions = self.kb.findConditions(prop)
        for condition in conditions:
            for rule in self.kb.getRules(condition):
                if rule != None:
                    newRule = rule.assign(condition, prop)
                    conditionIndex = rule.nodes.index(condition)
                    if newRule != None:
                        self.applyRule(newRule, [conditionIndex])

    def applyRule(self, rule, done=None, depth=0):
        if VERBOSE_DEBUG:
            d = "\t"*depth
            print d+"GOT RULE "+str(rule)
        if done == None:
            done = []
        found = False
        for nodeIndex in range(len(rule.nodes)):
            if VERBOSE_DEBUG:
                print d+"CHECKING: "+str(nodeIndex)+" CHECKED: "+str(done)
            if nodeIndex not in done:
                done.append(nodeIndex)#we have expanded this condition
                node = rule.nodes[nodeIndex]
                if found:
                    if node.abstract:
                        if VERBOSE_DEBUG:
                            print d+"ABSTRACT NODE"
                        break
                    self.addProp(node)#add the outcome
                elif node == "implies": found = True#apply the conditions
                else:
                    matches = self.kb.findMatches(node)#find matches for the rule
                    for match in matches:
                        if VERBOSE_DEBUG:
                            print d+"\t MATCH "+str(match)
                        newRule = rule.assign(node, match)
                        if newRule != None:
                            self.applyRule(newRule, list(done), depth+1)
                    break
        if VERBOSE_DEBUG:
            print d+"DONE\n"
        return#all done

