import json
from pyld import jsonld

#to_rdf and from_rdf
#owl?
#tuples not doc pls

UNKNOWN_OBJECT_NAME = "_:c14n"

def getTuplesFromFile(filename):
	f = open(filename, 'r').read()

	return getTuples(f, default=filename+"#")

def getTuples(doc, default=UNKNOWN_OBJECT_NAME):
	doc = json.loads(doc)
	normal = jsonld.normalize(doc, {'format': 'application/nquads'})

	normal = normal.replace("<", "")
	normal = normal.replace(">", "")	
	normal = normal.replace(UNKNOWN_OBJECT_NAME, default)
	normal = normal.split(" .\n")

	tuples = [line.split(" ") for line in normal if line != ""]
	
	for t in tuples:
		print("("+", ".join(t)+");")

	return tuples

if __name__ == '__main__':
	filename = "jsonExamples/burger.json"
	from sys import argv
	if len(argv) > 1:
		filename = argv[1]
	
	getTuplesFromFile(filename)
