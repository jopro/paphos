#Paphos - MIMIR v5.1#
### Problem And Probabilistic Heuristic Operation Solver ###
####An Experimental Logic Engine by Joshua Pratt (Started 2012)####

# This is now outdated, due to limitations of purely logical processing, it is still online for posterity and hopefully someone who attempts this will stumble on this and be saved a great deal of time #

##Motivation##
Many past logic engines have been powerful and effective but very few have made a substantial impact on our progress towards general intelligence as they have been restricted in many ways. They have not been able to be developed to approach new problems XXX


###Nodes###
In Paphos a Node (sometimes called an Atom) is a representation of an label for an idea.
Nodes are like words, they reference the idea that is represented by them and can be used to make statements about that idea.
For example when we talk about __John__, we don't actually have __John__ in the sentance,
we just use the word __John__ to talk about the guy that we both know as __John__.

Similarly a node is used to do the same thing inside the Paphos logic engine.

In Paphos a node is represented by any set of characters that does not contain whitespace or the '(' and ')' brackets.

For example, the following are valid node names:

	Bear
	Dog
	1337
	and
	http://thisurl.com/
	http://are_all_valid_nodes.org/

But the following are not:

	hell no
	why not me?
	http://this neither/
	;(

This means that you can use a word that you are familiar with to represent an idea or use a URI (like in the semantic web) so that the idea can have a universal meaning.

###Chains###
Chains are a way of representing statements in Paphos, they are a lot like sentances.
When chains are in an input file we end lines with a ';'.

An example of a chain is:

	(Aristotle is_a human)

Which means "Aristotle is [definitely] a human"
or "Everything that is true about all humans is true about Aristotle".

Most types of chains have no intrinsic meaning, they can be interpreted only by the rules that are created near them (we'll talk about rules later).

##Probability##

##Abstract Nodes and Rules##

##Activate##
'activate' is a node that tells the bot that the rest of the chain after it is a command to execute.

The syntax of __activate__ is as follows:

	(activate somefunction returnNode argumentNode1 argumentNode2)

This will run the function __somefunction__ with the arguments argumentNode1,
argumentNode2, etc. and return the value into the returnNode.

##Dependancies##
Requires a few things to make everything work:
    -pymongo

Optional things:
    -termcolor